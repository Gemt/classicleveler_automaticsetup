-- Author      : G3m7
-- Create Date : 5/31/2019 12:39:02 AM

--[[
    When visiting a trainer, and opening the trainer window, spells from your current level and 9 levels back will be trained.
    Spells further than 9 levels back are not checked.

    Skills are trained in the order they are written in the table below. 
    In other words, if you don't have enough money to buy all available, specified, skills, they
    will be prioritized from top to bottom.


    n="skillame":       The exact name of the spell to train. Case sensitive.
    r="rank":           The rank of the spell to train. This will either be an empty string ( "" ), or "Rank 1" etc
    Actionbar=1:        (Optional) This will place the newly trained spell on the actionbar specified. For valid values see https://i.imgur.com/IbP6QK0.jpg
    Macro="macroname":  (Optional) This will do a lookup in CLGuide_MacroTable (Macros.lua), and if a "macroname" is found,
                        that macro will be placed on the possition specified within the macro table.
    
    NB: Specifying Actionbar and Macro for the same spell is not supported. Either specify neither, OR Actionbar, OR Macro.

]]

CLGuide_SpellTable = {
    --[[
        EXAMPLE:
    {
        Lvl=0,
        Skills={
            {n="Apprentice Cook", r=""}, -- you can specify non class-skills as well, like cooking for example
            {n="Track Beasts", r="", Actionbar=48},
        }
    },
    {
        Lvl=4,
        Skills={
            {n="Serpent Sting", r="Rank 1", Macro="serpent"},
            {n="Aspect of the Monkey", r="", Macro="monkey"},
        }
    },
    ]]
}
