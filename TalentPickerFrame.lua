CLGuide_AutoPuttingTalents = false

function TalentPickerFrame_OnEvent(self, event, arg1)
    if CLGuide_TalentPicker_Talents == nil then return end
    if CLGuide_Options.ShowTalentPicker ~= true then return end

	if event == "PLAYER_LEVEL_UP" and arg1 > 9 then
        local talentClass = CLGuide_TalentPicker_Talents["class"]
        if talentClass == nil then return end
        if UnitClass("player") ~= talentClass then
            GuideWarning("AutomaticSetup/Talents.lua is configured for "..talentClass..", but player is "..UnitClass("player"))
            TalentPickerFrame:Hide();
            return
        end
        local talent = CLGuide_TalentPicker_Talents[tonumber(arg1)]
        if talent == nil then 
            GuidePrint("No talent specified for lvl "..tostring(arg1))
            return 
        end

        local name,icon=GetTalentInfo(talent.tab, talent.idx)
		TrainButton:SetText(name)
		TalentPickerFrame:Show()
	end
end

function TrainButtonOnClick()
	local lvl = UnitLevel("player");
	local talent = CLGuide_TalentPicker_Talents[tonumber(lvl)]
    if talent == nil then return end
    local talentClass = CLGuide_TalentPicker_Talents["class"]
    if UnitClass("player") ~= talentClass then
        GuideWarning("AutomaticSetup/Talents.lua is configured for "..talentClass..", but player is "..UnitClass("player"))
        TalentPickerFrame:Hide();
        return
    end
	GuidePrint("Learning "..talent.Name.." ("..talent.tab..","..talent.idx..")")
	LearnTalent(talent.tab, talent.idx);
    
    if talent.PlaceSpell ~= nil then
        table.insert(CLGuide_PlaceSpellQueue, {Spell=talent.PlaceSpell,Time=GetTime()+2})
    end

	TalentPickerFrame:Hide();
end
