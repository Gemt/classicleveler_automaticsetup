-- Author      : G3m7
-- Create Date : 5/31/2019 10:12:25 AM

--[[
    Talents are indexed as follows:
    Tab 1 is the leftmost tab, tab 2 is the middle tab, tab 3 is the rightmost tab.
    Indexes goes from left to right, and then down. Same way you read a book. So the top-left
    talent is index 1, followed by 2. Once you reach the rightmost talent of the first tier,
    the next index is the leftmost talent on the second row.
    Each tab has their index start at 1.
    
    The Name does not matter for the talent choice. It was previously used to show the name of 
    the talent on the button you press to choose the talent, but is no longer needed. I am keeping it
    so the table is self-documenting in a sense.

    See: https://wowwiki.fandom.com/wiki/API_LearnTalent
]]
CLGuide_TalentPicker_Talents = {
    
    -- This value is used to assert the currently played class is in fact this class.
    -- If it is not, the talentPicker will not work.
    -- CHANGE THIS TO YOUR CLASS; AND DONT REMOVE IT
    ["class"] = "Hunter",

    -- EXAMPLES:
	--[10] = {Name="Imp Aspect of Hawk", tab=1,idx=1},
	--[11] = {Name="Imp Aspect of Hawk", tab=1,idx=1},
	--[12] = {Name="Imp Aspect of Hawk", tab=1,idx=1},
	--[13] = {Name="Imp Aspect of Hawk", tab=1,idx=1},
	--[14] = {Name="Imp Aspect of Hawk", tab=1,idx=1},
	--[15] = {Name="Thick Hide", tab=1,idx=5},
	--[16] = {Name="Thick Hide", tab=1,idx=5},
	--[17] = {Name="Thick Hide", tab=1,idx=5},

}