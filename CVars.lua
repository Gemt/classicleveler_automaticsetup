-- Author      : G3m7
-- Create Date : 5/31/2019 12:03:55 AM

local function CLGuide_pcall(cvar, value)
    if pcall(SetCVar, cvar, value) then
        else DEFAULT_CHAT_FRAME:AddMessage("|cffff0000Cvar not found: "..cvar)
    end
end

function CLGuide_SetupCVars()
    GuideWarning("Starting setting CVars")

    -- Useful CVars (many of them don't work in 1.12)
    -- Remove -- at start of line to use the cvar
    --CLGuide_pcall("useuiscale", 1)              -- enables UI Scale
    --CLGuide_pcall("uiscale", 0.64)              -- UI Scale, lower value means "smaller" UI Elements
    --CLGuide_pcall("cameraDistanceMaxZoomFactor", 2.6) -- how far out you can zoom camera
    --CLGuide_pcall("cameraSmoothStyle", 0)       -- camera follow style: never
    --CLGuide_pcall("autoLootDefault", 1)         -- turns auto loot on, no shift-click needed
    --CLGuide_pcall("autoLootRate", 0)            -- milliseconds between autoloot-loots
    --CLGuide_pcall("deselectOnClick", 1)         -- turns off "sticky targetting"
    --CLGuide_pcall("profanityFilter", 0)         -- off
    --CLGuide_pcall("showTutorials", 0)           -- none of those exclamation marks
    --CLGuide_pcall("SpellQueueWindow", 150)      -- google it
    --CLGuide_pcall("alwaysShowActionBars", 1)    -- 
    --CLGuide_pcall("xpBarText", 1)               -- show currentxp/totalxp on your xp bar
    --CLGuide_pcall("nameplateMaxDistance", 100)  -- show nameplates 100yd away
    --CLGuide_pcall("mouseSpeed", 0.5)            --
    --CLGuide_pcall("UnitNameNPC", 1)             -- show name of NPCs
    --CLGuide_pcall("ShowTargetOfTarget", 1)      -- show target of target
    --CLGuide_pcall("InstantQuestText", 1)        -- instant quest text
    
    GuideWarning("Finished setting CVars")
end