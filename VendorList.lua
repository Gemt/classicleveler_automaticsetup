-- Author      : G3m7
-- Create Date : 5/4/2019 9:44:47 PM

--[[
    ["nameOfItem"] = minimumLevelToVendorAt

    Specify the item name to vendor within the square brackets. 
    The number is compared to your character level. If you are equal to, or higher level, than
    this number, the item is vendored. 
    In other words, when the number is 1, the item will always be vendored. 
    If the number is 10, the item will not be vendored when you are level 1-9, 
    but when you are level 10 or higher it will be vendored.

    NB: The name is case-sensitive and must be spelled exactly right. Otherwise it will not be vendored.
]]

CLGuide_VendorList = {
-- EXAMPLES:
--["Gooey Spider Leg"] = 30, --duskwood quest
--["Small Spider Leg"] = 10, -- COOKING QUEST IN TELDRASSIL
--["Worn Dagger"] = 5,
--["Rellian Greenspyre"] = 1,
} 

-- small furry paw