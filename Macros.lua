-- Author      : G3m7
-- Create Date : 5/31/2019 12:03:42 AM

--[[
    How to add your own macros:
    Schema:
        {"macroName", "macroIcon", "macroText", perCharacter, actionbarNum, onInitalLogin},
    Example Macro:
        {"bloodthirst", "spell_nature_bloodlust", "#showtooltip bloodthirst\n/startattack\n/cast bloodthirst", 1, 2, 1},

    macroName:      A string which will be the name of the macro ingame. NB: MUST BE A UNIQUE NAME
    macroIcon:      The icon which will be used for the macro. See https://www.wowhead.com/icons
    macroText:      The actual macro. Each line of the macro must end with \n, except the last line
    perCharacter:   
                    0: the macro ends up in the "general macros" tab.
                    1: the macro ends up in "character macros" tab. 
                    (keep in mind you can have a maximum of 18 general and 18 character specific macros)
    actionbarNum:   (optional) Action bar to put the macro on. For valid values see https://i.imgur.com/IbP6QK0.jpg
    onInitialLogin: (optional)
                    1: the macro will automatically be placed at actionbarNum when you login


    Note regarding actionbarNum and onInitialLogin:
    While both are optional, if you specify actionbarNum, you have the option of referencing macros in this table by macroName
    from the SkillTable, in which case the macro will be placed on your action bars when the spell is automatically learned from a trainer.
]]
CLGuide_MacroTable = {
    -- EXAMPLES:
    --{"raptorStrike", "INV_MISC_QUESTIONMARK", "#showtooltip raptor strike\n/stopcasting\n/cast raptor strike\n/cast Mongoose Bite\n/startattack", 1, 2, 1},
    --{"autoshot", "inv_weapon_bow_05", "#showtooltip Auto Shot\n/cast !Auto Shot", 1, 6, 1},
}

