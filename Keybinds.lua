-- Author      : G3m7
-- Create Date : 5/31/2019 12:03:34 AM

function CLGuide_SetupKeybinds()
    GuideWarning("Starting setting up keybinds")
    
    -- These 6 lines makes all action bars shown
    SetActionBarToggles(1,1,1,1,1)
    SHOW_MULTI_ACTIONBAR_1=1
    SHOW_MULTI_ACTIONBAR_2=1
    SHOW_MULTI_ACTIONBAR_3=1
    SHOW_MULTI_ACTIONBAR_4=1
    pcall(InterfaceOptions_UpdateMultiActionBars)
    -------------------------------------------------------
    
    -- EXAMPLE:
    -- SetBinding("F1", "ACTIONBUTTON6")
    
    
    --SaveBindings(1)
    GuideWarning("Finished setting up keybinds")
end